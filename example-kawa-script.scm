;;  This file is part of ivy-util and shows an example of usage.
;;  Copyright (C) 2019 spellcard199

;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU Affero General Public License as
;;  published by the Free Software Foundation, either version 3 of the
;;  License, or (at your option) any later version.

;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU Affero General Public License for more details.

;;  You should have received a copy of the GNU Affero General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Edit the next 2 lines.
(define ivy-cache-dir ::<string> #!null)
(define ivy-dest-dir  ::<string> #!null)
;; E.g.:
;; (define ivy-cache-dir ::<string> "/tmp/ivy-util-example-cache")
;; (define ivy-dest-dir  ::<string> "/tmp/ivy-util-example-dest")

;; Tell user about warnings.
(display "----------\n")
(display "[example-kawa-script.scm] (Note that when you tried to run this script the kawa type checker produced warnings. The reason is explained in the comments in the script's source.)\n")
(display "----------\n")
;; Check you actually edited the definition aboves.
(when (or (not ivy-cache-dir)
          (not ivy-cache-dir))
  (display "[example-kawa-script.scm] You have to edit the definitions for `ivy-cache-dir' and `ivy-dest-dir' in the source of `example-kawa-script.scm' before this can be run.\n")
  (display "----------\n")
  (exit 1))

(load "ivy-util.scm")
(import (ivy-util))

;; Download jsoup and its dependencies from maven central and put them
;; in their own java.net.URLClassLoader
(define cloader-jsoup ::java.net.URLClassLoader
  (ivy-get-deps-make-cl ivy-cache-dir
                        ivy-dest-dir
                        '(("org.jsoup" "jsoup" "1.8.3"))))
(define Jsoup ::java.lang.Class
  (cloader-jsoup:loadClass "org.jsoup.Jsoup"))

;; Notice that using kawa symbols as classes is a kawa hack that
;; works, but doesn't satisfy the type checker, that produces warnings
;; since it doesn't recognize them as actual classes.
;; If instead you want to use the loaded classes in a library or
;; silence warnings, you should use the more verbose but functionally
;; equivalent java reflection api.

(define doc ((Jsoup:connect "https://www.gnu.org/software/kawa/"):get))
;; Extract Information
(define els (doc:getElementsContainingOwnText "Features"))
;; Display result
(newline)
(display "#### Begin of result ####") (newline)
(display els) (newline)
(display "##### End of result #####") (newline)
