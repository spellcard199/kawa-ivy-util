// IvyUtil wraps functionalities of Apache Ivy using its java api.
// Copyright (C) 2019 spellcard199
//
// IvyUtil is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// IvyUtil is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


package com.gitlab.spellcard199;

import org.apache.ivy.Ivy;
import org.apache.ivy.core.module.descriptor.DefaultDependencyDescriptor;
import org.apache.ivy.core.module.descriptor.DefaultModuleDescriptor;
import org.apache.ivy.core.module.descriptor.ModuleDescriptor;
import org.apache.ivy.core.module.id.ModuleRevisionId;
import org.apache.ivy.core.report.ResolveReport;
import org.apache.ivy.core.resolve.ResolveOptions;
import org.apache.ivy.core.retrieve.RetrieveOptions;
import org.apache.ivy.core.retrieve.RetrieveReport;
import org.apache.ivy.core.settings.IvySettings;
import org.apache.ivy.plugins.resolver.IBiblioResolver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.stream.Collectors;


public class IvyUtil {

    public File ivyCache;
    public File destDir;

    public
    IvyUtil
            (File ivyCache, File destDir)
    {
        this.ivyCache = ivyCache;
        this.destDir  = destDir;
    }

    public ArrayList<File>
    retrieveDeps
            (String groupId, String artifactId, String version)
    {
        // Adapted from:
        // https://stackoverflow.com/questions/15598612/simplest-ivy-code-to-programmatically-retrieve-dependency-from-maven-central

        ////////// DefaultModuleDescriptor dmd, ModuleRevisionId mri //////////

        // give it some related name (so it can be cached)
        ModuleRevisionId defaultmri = ModuleRevisionId.newInstance(
                groupId,
                artifactId + "-envelope",
                version
        );
        // 1st create an ivy module (this always(!) has a "default" configuration already)
        DefaultModuleDescriptor dmd =
                DefaultModuleDescriptor.newDefaultInstance( defaultmri );

        // 2. add dependencies for what we are really looking for
        ModuleRevisionId mri = ModuleRevisionId.newInstance(
                groupId,
                artifactId,
                version
        );

        // don't go transitive here, if you want the single artifact
        DefaultDependencyDescriptor ddd =
                new DefaultDependencyDescriptor(
                        dmd, mri, false, false, false);

        // map to master to just get the code jar. See generated ivy module xmls from maven repo
        // on how configurations are mapped into ivy. Or check
        // e.g. http://lightguard-jp.blogspot.de/2009/04/ivy-configurations-when-pulling-from.html
        ddd.addDependencyConfiguration("default", "master");
        dmd.addDependency(ddd);

        ////////// Resolve Options //////////

        // You always need to resolve before you can retrieve
        ResolveOptions ro = new ResolveOptions();
        // Important:
        // - if you resolve by ModuleRevisionId: .setTransitive actually has effect
        // - if you resolve by ModuleDescriptor: .setTransitive has _no_ effect
        ro.setTransitive(true);
        // if set to false, nothing will be downloaded
        ro.setDownload(true);

        ////////// Ivy instance //////////

        // create an ivy instance
        IvySettings ivySettings = new IvySettings();
        ivySettings.setDefaultCache(this.ivyCache);

        // use the biblio resolver, if you consider resolving
        // POM declared dependencies
        IBiblioResolver br = new IBiblioResolver();
        br.setM2compatible(true);
        br.setUsepoms(true);
        br.setName("central");

        ivySettings.addResolver(br);
        ivySettings.setDefaultResolver(br.getName());

        Ivy ivy = Ivy.newInstance(ivySettings);

        ////////// Resolve //////////

        ResolveReport rr = null;
        try {
            // rr = ivy.resolve(dmd,ro);
            rr = ivy.resolve(mri, ro, true );
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        if (rr == null) {
            throw new RuntimeException("[ERROR] rr is null.");
        } else if (rr.hasError()) {
            throw new RuntimeException(rr.getAllProblemMessages().toString());
        }

        ////////// Retrieve //////////

        ModuleDescriptor m = rr.getModuleDescriptor();

        ArrayList<File> files = new ArrayList<>();

        try {
            RetrieveReport retRep = ivy.retrieve(
                    m.getModuleRevisionId(),
                    new RetrieveOptions()
                            // this is from the envelop module
                            .setConfs(new String[]{"default"})
                            .setDestArtifactPattern(this.destDir.getAbsolutePath() + "/[artifact](-[classifier]).[ext]")
            );
            files.addAll(retRep.getUpToDateFiles());
            files.addAll(retRep.getRetrievedFiles());
            files.addAll(retRep.getCopiedFiles());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return files;
    }

    public URLClassLoader
    newURLClassLoaderFromFiles
            (ArrayList<File> files)
    {
        ArrayList<URL>
                fileUrlArrList =
                files.stream().map(f -> {
                    try {
                        return f.toURI().toURL();
                    } catch (java.net.MalformedURLException mue) {
                        System.out.println(String.format(
                                "[ERROR - newURLClassLoaderFromFiles] java.net.MalformedURLException for file: %s",
                                f.toString()));
                        return null;
                    }
                }).collect(Collectors.toCollection(ArrayList::new));
        URL[] fileUrlArr = new URL[fileUrlArrList.size()];
        fileUrlArrList.toArray(fileUrlArr);
        return URLClassLoader.newInstance(fileUrlArr);
    }

    public URLClassLoader
    newURLClassLoaderFromModuleRevisionIds
            (java.util.Collection<ModuleRevisionId> moduleRevIds)
    {

        ArrayList<File> files =
                moduleRevIds.parallelStream().flatMap(
                        mri -> retrieveDeps(
                                mri.getOrganisation(),
                                mri.getName(),
                                mri.getRevision()).stream()
                ).collect(Collectors.toCollection(ArrayList::new));

        return newURLClassLoaderFromFiles(files);
    }

    public URLClassLoader
    newURLClassLoaderFromArtifact
            (String groupId, String artifactId, String version)
    {

        ArrayList<File> localFiles = retrieveDeps(groupId, artifactId, version);
        return newURLClassLoaderFromFiles(localFiles);
    }
}
