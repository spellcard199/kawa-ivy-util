;; ivy-util.scm wraps some functionalities of Apache Ivy and makes them readily available to kawa scheme.

;; Copyright (C) 2019 spellcard199

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-library (ivy-util)

  (export ivy-get-deps-make-cl)

  (import (kawa base))

  (define IvyUtil-jar-relative-path-str ::java.lang.String
    "target/ivy-util-0.1-SNAPSHOT-jar-with-dependencies.jar")

  (define IvyUtil-jar-url ::java.net.URL
    (let* ((cur-file-str
            ::java.lang.String
            (*:getSourceAbsPathname
             ((gnu.expr.ModuleManager:getInstance):findWithClassName "ivy-util")))

           (cur-file (java.io.File cur-file-str))

           (parent-dir ::java.lang.String
                       (cur-file:getParent))

           (IvyUtil-jar-file-str ::java.lang.String
                                 (java.lang.String:join
                                  cur-file:separator
                                  (java.util.Arrays:asList
                                   parent-dir
                                   IvyUtil-jar-relative-path-str))))
      
      (((java.io.File IvyUtil-jar-file-str):toURI):toURL)))

  (define ivy-util-cl ::java.net.URLClassLoader
    (java.net.URLClassLoader
     (java.net.URL[] IvyUtil-jar-url)))

  (define com.gitlab.spellcard199.IvyUtil
    ::java.lang.Class
    (ivy-util-cl:loadClass
     "com.gitlab.spellcard199.IvyUtil"))

  (define org.apache.ivy.core.module.id.ModuleRevisionId
    ::java.lang.Class
    (ivy-util-cl:loadClass
     "org.apache.ivy.core.module.id.ModuleRevisionId"))

  (define (triplet->ModuleRevisionId
           triplet ::<list>)
    ;; ::org.apache.ivy.core.module.id.ModuleRevisionId
    (when (not (equal? (length triplet) 3))
      (throw (java.lang.Exception
              (java.lang.String:format
               "[ERROR] `triplet' argument to `triplet->ModuleRevisionId':\n\
              - should be  : a list of 3 Strings\n\
              - actually is: %A"
               triplet))))
    (let ((organization ::java.lang.String (car   triplet))
          (name         ::java.lang.String (cadr  triplet))
          (revision     ::java.lang.String (caddr triplet)))
      (org.apache.ivy.core.module.id.ModuleRevisionId:newInstance
       organization name revision)))

  (define (ivy-get-deps-make-cl ivy-cache-dir ::<string>
                                dest-dir      ::<string>
                                triplets      ::<list>)

    ;; Validate `ivy-cache-dir' and `dest-dir'
    (cond ((not (string? ivy-cache-dir))
           (throw
            (java.lang.Exception
             (format "[ERROR - ivy-util-set-dirs] Argument 1 (ivy-cache-dir) must be a string."))))
          ((not (string? dest-dir))
           (throw
            (java.lang.Exception
             (format "[ERROR - ivy-util-set-dirs] Argument 2 (dest-dir) must be a string."))))) 

    (define triplets-doc
      "`triplets' must be a list of lists of strings. Inner lists must:\n\
      - have length == 3\n\
      - contain only strings")

    ;; Validating `triplets'
    (for-each
     (lambda (triplet ::<list>)
       (when (or
              ;; length must be 3
              (not (equal? (length triplet) 3))
              ;; only strings allowed
              (member #f
                      (map (lambda (x) (string? x))
                           triplet)))
         (java.lang.Exception
          (format "~A\n`triplets' argument actually passed was:\n~S"
                  triplets-doc triplets))))
     triplets)

    ;; Retrieve deps and make ClassLoader from resulting jars
    (let* ((inst     ((ivy-util-cl:loadClass
                       "com.gitlab.spellcard199.IvyUtil")
                      (java.io.File ivy-cache-dir)
                      (java.io.File dest-dir)))
           (clas     ::java.lang.Class
                     (*:getClass inst))
           (method   ::java.lang.reflect.Method
                     (*:getDeclaredMethod
                      clas
                      "newURLClassLoaderFromModuleRevisionIds"
                      java.util.Collection:class)))
      (method:invoke inst
                     (map triplet->ModuleRevisionId
                          triplets))))
  )
